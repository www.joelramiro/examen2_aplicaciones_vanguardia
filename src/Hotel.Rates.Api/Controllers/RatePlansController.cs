﻿using Hotel.Rates.Core.Interfaces;
using Hotel.Rates.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Rates.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RatePlansController : BaseApiController
    {
        private readonly IRatePlanService ratePlanService;

        public RatePlansController(IRatePlanService ratePlanService)
        {
            this.ratePlanService = ratePlanService;
        }
        
        [HttpGet]
        public IActionResult Get()
        {
            var result = this.ratePlanService.GetRatePlans();
            return GetResult(result);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = this.ratePlanService.GetRatePlanById(id);
            return GetResult(result);
        }
    }
}
