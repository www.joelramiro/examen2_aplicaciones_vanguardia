﻿using Hotel.Rates.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hotel.Rates.Data.Repositories
{
    public class RatePlanRoomRepository : IRatePlanRoomRepository
    {
        private readonly InventoryContext inventoryContext;

        public RatePlanRoomRepository(InventoryContext inventoryContext)
        {
            this.inventoryContext = inventoryContext;
        }

        public void Update(RatePlanRoom ratePlanRoom)
        {
            this.inventoryContext.Update(ratePlanRoom);
            this.inventoryContext.SaveChanges();
        }
    }
}
