﻿using Hotel.Rates.Core.DTO;
using Hotel.Rates.Core.Interfaces;
using Hotel.Rates.Core.Rules.ReservationRules;
using Hotel.Rates.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hotel.Rates.Core.Rules.RulesService
{
    public class IntervalRatePlanRuleService : IReservationPlanRule
    {
        private readonly IRepository<IntervalRatePlan, int> repository;

        public IntervalRatePlanRuleService(IRepository<IntervalRatePlan, int> repository)
        {
            this.repository = repository;
        }

        public RatePlanType ratePlan => RatePlanType.Interval;

        public ServiceResult<double> ApplyAndCalculatePrice(ReservationModelDTO reservationModelDTO)
        {
            var rate = this.repository.GetById(reservationModelDTO.RatePlanId);

            if (rate == null)
            {
                throw new ApplicationException("No se encontro el plan");
            }
            var days = (reservationModelDTO.ReservationEnd - reservationModelDTO.ReservationStart).Days;

            if (days < rate.IntervalLength)
            {
                throw new ApplicationException("No cumple con el requisito minimo de dias");
            }

            
            var daylyPrice = rate.IntervalLength / rate.Price;

            return ServiceResult<double>.SuccessResult(days * daylyPrice);
        }
    }
}
