﻿using Hotel.Rates.Core.DTO;
using Hotel.Rates.Core.Interfaces;
using Hotel.Rates.Core.Rules.ReservationRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hotel.Rates.Core.Rules.RulesService
{
    public class RulesServiceEngine : IRulesServiceEngine
    {
        private readonly IEnumerable<IReservationPlanRule> reservationPlanRules;
        private readonly IRatePlanRepository ratePlan;

        public RulesServiceEngine(IEnumerable<IReservationPlanRule> reservationPlanRules, IRatePlanRepository ratePlan)
        {
            this.reservationPlanRules = reservationPlanRules;
            this.ratePlan = ratePlan;
        }

        public ServiceResult<double> AplyRulesAndGetPrice(ReservationModelDTO reservationModelDTO)
        {
            var ratePlan = this.ratePlan.GetByIdWithDetail(reservationModelDTO.RatePlanId);
            if (ratePlan == null)
            {
                throw new ApplicationException($"No se encontro un rateplan con el Id [{reservationModelDTO.RatePlanId}]");
            }

            var room = ratePlan.RatePlanRooms
                .FirstOrDefault(r => r.RoomId == reservationModelDTO.RoomId && r.RatePlanId == reservationModelDTO.RatePlanId);

            if (room == null)
            {
                throw new ApplicationException($"No se encontro la habitacion solicitada");
            }


            var canReserve = ratePlan.Seasons
                .Any(s => reservationModelDTO.ReservationStart >= s.StartDate && reservationModelDTO.ReservationStart <= s.EndDate);

            if (!canReserve)
            {
                throw new ApplicationException($"No se encuentra una season para el plan de fechas solicitadas");
            }

            var isRoomAvailable = room.Room.Amount > 0 &&
                room.Room.MaxAdults >= reservationModelDTO.AmountOfAdults &&
                room.Room.MaxChildren >= reservationModelDTO.AmountOfChildren;

            if (!isRoomAvailable)
            {
                throw new ApplicationException($"No hay habitacion disponible para el plan solicitado");
            }

            var rule = this.reservationPlanRules.FirstOrDefault(d => (int)d.ratePlan == ratePlan.RatePlanType);

            if (rule == null)
            {
                return ServiceResult<double>.ErrorResult("No se encontro ninguna regla para este caso");
            }

            return rule.ApplyAndCalculatePrice(reservationModelDTO);
        }
    }
}
