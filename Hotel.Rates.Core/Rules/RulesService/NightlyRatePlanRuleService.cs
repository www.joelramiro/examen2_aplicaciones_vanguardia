﻿using Hotel.Rates.Core.DTO;
using Hotel.Rates.Core.Interfaces;
using Hotel.Rates.Core.Rules.ReservationRules;
using Hotel.Rates.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hotel.Rates.Core.Rules.RulesService
{
    public class NightlyRatePlanRuleService : IReservationPlanRule
    {
        private readonly IRatePlanRepository ratePlanRepository;

        public NightlyRatePlanRuleService(IRatePlanRepository ratePlanRepository)
        {
            this.ratePlanRepository = ratePlanRepository;
        }
        public RatePlanType ratePlan => RatePlanType.Nightly;

        public ServiceResult<double> ApplyAndCalculatePrice(ReservationModelDTO reservationModelDTO)
        {
            var ratePlan = this.ratePlanRepository.GetByIdWithDetail(reservationModelDTO.RatePlanId);
            var days = (reservationModelDTO.ReservationEnd - reservationModelDTO.ReservationStart).TotalDays;
            return ServiceResult<double>.SuccessResult(days * ratePlan.Price);
        }
    }
}
