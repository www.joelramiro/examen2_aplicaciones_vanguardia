﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hotel.Rates.Core.Enums
{
    public enum ResponseCode
    {
        Success,
        Error,
        InternalServerError = 500,
        NotFound
    }
}
