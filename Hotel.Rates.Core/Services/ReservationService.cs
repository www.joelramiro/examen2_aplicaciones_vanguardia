﻿using Hotel.Rates.Core.DTO;
using Hotel.Rates.Core.Interfaces;
using Hotel.Rates.Core.Rules.ReservationRules;
using Hotel.Rates.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hotel.Rates.Core.Services
{
    public class ReservationService : IReservationService
    {
        private readonly IRepository<RatePlan, int> repository;
        private readonly IRatePlanRoomRepository roomRepository;
        private readonly IRatePlanRepository ratePlanRepository;
        private readonly IRepository<IntervalRatePlan, int> intervalRatePlanRepository;
        private readonly IRulesServiceEngine rulesServiceEngine;

        public ReservationService(IRepository<RatePlan, int> repository,
            IRatePlanRoomRepository roomRepository,
            IRatePlanRepository ratePlanRepository, 
            IRepository<IntervalRatePlan, int> intervalRatePlanRepository, 
            IRulesServiceEngine rulesServiceEngine)
        {
            this.repository = repository;
            this.roomRepository = roomRepository;
            this.ratePlanRepository = ratePlanRepository;
            this.intervalRatePlanRepository = intervalRatePlanRepository;
            this.rulesServiceEngine = rulesServiceEngine;
        }



        public ServiceResult<ReservationDTO> CreateReservation(ReservationModelDTO reservationModelDTO)
        {
            var ratePlan = this.repository.GetById(reservationModelDTO.RatePlanId);
            if (ratePlan == null)
            {
                return ServiceResult<ReservationDTO>.ErrorResult("No se encontro el plan solicitado");
            }

            var price = ApplyRules(reservationModelDTO, ratePlan);
            if (price == -1)
            {
                return ServiceResult<ReservationDTO>.ErrorResult("No aplico ninguna regla");
            }

            var room = ratePlan.RatePlanRooms
                .First(r => r.RoomId == reservationModelDTO.RoomId && r.RatePlanId == reservationModelDTO.RatePlanId);

            room.Room.Amount -= 1;
            this.roomRepository.Update(room);

            return ServiceResult<ReservationDTO>.SuccessResult(new ReservationDTO { Price = price });
        }

        private double ApplyRules(ReservationModelDTO reservationModelDTO, RatePlan ratePlan)
        {
            var result = this.rulesServiceEngine.AplyRulesAndGetPrice(reservationModelDTO);

            if (!string.IsNullOrEmpty(result.Error))
            {
                return -1;
            }
            return result.Result;
        }
    }
}
