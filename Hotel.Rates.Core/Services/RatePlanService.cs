﻿using Hotel.Rates.Core.Interfaces;
using Hotel.Rates.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Hotel.Rates.Core.DTO;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Hotel.Rates.Core.Services
{
    public class RatePlanService : IRatePlanService
    {
        private readonly IRatePlanRepository ratePlanRepositoryBase;

        public RatePlanService(IRatePlanRepository ratePlanRepositoryBase)
        {
            this.ratePlanRepositoryBase = ratePlanRepositoryBase;
        }

        public ServiceResult<RatePlanDTO> GetRatePlanById(int id)
        {
            var rate = this.ratePlanRepositoryBase.GetByIdWithDetail(id);

            if (rate == null)
            {
                return ServiceResult<RatePlanDTO>.ErrorResult($"Not found a rate with Id [{id}]");
            }

            var resultRate = new RatePlanDTO 
            {
                Id = rate.Id,
                Name = rate.Name,
                Price = rate.Price,
                RatePlanType = rate.RatePlanType,
                RatePlanRooms = rate.RatePlanRooms.Select(r => new RoomDTO
                {
                    Amount = r.Room.Amount,
                    MaxAdults = r.Room.MaxAdults,
                    MaxChildren = r.Room.MaxChildren,
                    Name = r.Room.Name,

                }).ToList(),
                Seasons = rate.Seasons.Select(s => new SeasonDTO
                {
                    Id = s.Id,
                    StartDate = s.StartDate,
                    EndDate = s.EndDate,
                }).ToList(),
            };

            return ServiceResult<RatePlanDTO>.SuccessResult(resultRate);
        }

        public ServiceResult<IReadOnlyList<RatePlanDTO>> GetRatePlans()
        {
            var rates = this.ratePlanRepositoryBase.GetAllWithDetail()
                .Select(d => new RatePlanDTO
                {
                    Id = d.Id,
                    Name = d.Name,
                    Price = d.Price,
                    RatePlanType = d.RatePlanType,
                    RatePlanRooms = d.RatePlanRooms.Select(r => new RoomDTO
                    {
                        Amount = r.Room.Amount,
                        MaxAdults = r.Room.MaxAdults,
                        MaxChildren = r.Room.MaxChildren,
                        Name = r.Room.Name,

                    }).ToList(),
                    Seasons = d.Seasons.Select(s => new SeasonDTO
                    {
                        Id = s.Id,
                        StartDate = s.StartDate,
                        EndDate = s.EndDate,
                    }).ToList(),
                });

            return ServiceResult<IReadOnlyList<RatePlanDTO>>.SuccessResult(rates.ToList());
        }
    }
}
