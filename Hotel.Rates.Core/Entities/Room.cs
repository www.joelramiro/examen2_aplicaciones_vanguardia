﻿using Hotel.Rates.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hotel.Rates.Data
{
    public class Room : BaseEntity<int>
    {
        public Room()
        {
            RatePlanRooms = new List<RatePlanRoom>();
        }

        public int MaxAdults { get; set; }

        public int MaxChildren { get; set; }

        public string Name { get; set; }

        public int Amount { get; set; }

        public ICollection<RatePlanRoom> RatePlanRooms { get; set; }

    }
}
