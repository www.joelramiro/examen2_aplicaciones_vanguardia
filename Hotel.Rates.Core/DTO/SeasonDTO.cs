﻿using System;

namespace Hotel.Rates.Core.DTO
{
    public class SeasonDTO
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}