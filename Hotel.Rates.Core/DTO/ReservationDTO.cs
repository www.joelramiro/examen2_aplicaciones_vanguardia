﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hotel.Rates.Core.DTO
{
    public class ReservationDTO
    {
        public double Price { get; set; }
    }
}
