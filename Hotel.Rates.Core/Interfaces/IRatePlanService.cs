﻿using Hotel.Rates.Core.DTO;
using Hotel.Rates.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hotel.Rates.Core.Interfaces
{
    public interface IRatePlanService
    {
        ServiceResult<IReadOnlyList<RatePlanDTO>> GetRatePlans();
        ServiceResult<RatePlanDTO> GetRatePlanById(int id);
    }
}
