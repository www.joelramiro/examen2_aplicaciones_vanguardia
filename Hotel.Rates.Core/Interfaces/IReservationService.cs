﻿using Hotel.Rates.Core.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hotel.Rates.Core.Interfaces
{
    public interface IReservationService
    {
        ServiceResult<ReservationDTO> CreateReservation(ReservationModelDTO reservationModelDTO);
    }
}
