﻿using Hotel.Rates.Core.DTO;
using Hotel.Rates.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hotel.Rates.Core.Rules.ReservationRules
{
    public interface IReservationPlanRule
    {
        public RatePlanType ratePlan { get; }

        public ServiceResult<double> ApplyAndCalculatePrice(ReservationModelDTO reservationModelDTO);
    }
}
